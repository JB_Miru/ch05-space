﻿#pragma strict

var speed = 50;
var explosion : Transform;
var sound : AudioClip;
var sonnd1 : AudioClip;

function Update () 
{
	var amtToMove = speed * Time.deltaTime;
	
	transform.Translate(Vector3.right * amtToMove);
	
	if(transform.position.x > 37) {
		Destroy(gameObject);
	}
}

function OnTriggerEnter(coll : Collider)
{
	if( (jsGameManager.state == STATE.ENTER) ||
		(jsGameManager.state == STATE.WAIT) )
	{
		return;
	}
	
	Destroy(gameObject);
	
	if(coll.gameObject.tag == "ASTEROID") {
		Instantiate(explosion, transform.position, Quaternion.identity);
		//AudioSource.PlayClipAtPoint(sound, transform.position);qkrwoqja rkdmfgksmf rhdghks
		
		jsGameManager.state = STATE.HIT;
		jsGameManager.transPos = coll.transform.position;
		
		coll.SendMessage("SetAsteroid", SendMessageOptions.DontRequireReceiver);
	}
	
	if(coll.gameObject.tag == "BONUS") {
		//AudioSource.PlayClipAtPoint(sound1, transform.position);
		jsGameManager.bonusNum = coll.gameObject.tag.Substring(5,1);
		jsGameManager.state = STATE.BONUS;
		Destroy(coll.gameObject);
	}
}