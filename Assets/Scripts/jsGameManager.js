﻿#pragma strict

var gunship : Transform;
var asteroid : Transform;
var bonus : Transform;
var explosion : Transform;
var sound : AudioClip;

var textScore1 : GUIText;
var textScore2 : GUIText;
var textScore3 : GUIText;
var textScore4 : GUIText;
var textScore5 : GUIText;

var textMsg : TextMesh;

private var shipCnt = 5;
private var asterCnt = 0;
private var score = 0;
private var bonusCnt = 0;
private var stageNum = 1;

static var bonusNum;
private var isAsteroid = true;

enum STATE { START, CLEAR, OVER, HIT, BONUS, DESTROY, IDLE, ENTER, WAIT};
static var state = STATE.START;

static var transPos : Vector3;
static var undeadCnt : int;

function Start () {

}

function Update () {
	switch(state) {
	case STATE.START :
		StageStart();
		break;
		
	case STATE.CLEAR :
		StageClear();
		break;
		
	case STATE.OVER :
		GameOver();
		break;
		
	case STATE.HIT :
		AsteroidHit();
		break;
		
	case STATE.BONUS :
		ProcessBonus();
		break;
		
	case STATE.DESTROY :
		GunshipDestroy();
		break;
	}
	
	Debug.Log(state);
}

function StageStart() {
}

function StageClear() {
}

function GameOver() {
}

function AsteroidHit() {
}

function ProcessBonus() {
}

function GunshipDestroy() {
}

function OnGUI() {
	textScore1.text = "Stage : " + stageNum;
	textScore2.text = "Ship : " + shipCnt;
	textScore3.text = "Score : " + score;
	textScore4.text = "Aster : " + asterCnt;
	textScore5.text = "Bonus : " + bonusCnt;
}