﻿#pragma strict

private var speedX : float;
private var speedY : float;

private var rotX : float;
private var rotY : float;
private var rotZ : float;

function Start () 
{
	SetAsteroid();
}

function Update () 
{
	var amtX = speedX * Time.deltaTime;
	var amtY = speedY * Time.deltaTime;

	var amtRotX = rotX * Time.deltaTime;	
	var amtRotY = rotY * Time.deltaTime;
	var amtRotZ = rotZ * Time.deltaTime;

	transform.Rotate(Vector3(amtRotX, amtRotY, amtRotZ), Space.Self);
	transform.Translate(Vector3(-amtX, amtY,0), Space.World);

	if(transform.position.x < -37) {
		Destroy(gameObject);
	}

	if( (transform.position.y < -22) || (transform.position.y > 22) ) {
		transform.position.y = -transform.position.y;
	}
}

function SetAsteroid()
{
	speedX = Random.Range(20f, 30f);
	speedY = Random.Range(-10f, 10f);
	
	var posX = Random.Range(40f, 55f);
	var posY = Random.Range(-16f, 17f);
	
	rotX = Random.Range(20f, 30f);
	rotY = Random.Range(20f, 30f);
	rotZ = Random.Range(20f, 30f);
	
	var scaleX = Random.Range(0.8f, 1.5f);
	var scaleY = Random.Range(0.8f, 1.5f);
	var scaleZ = Random.Range(0.8f, 1.5f);
	
	transform.position = Vector3(posX, posY,0);
	transform.localScale = Vector3(scaleX, scaleY, scaleZ);
}