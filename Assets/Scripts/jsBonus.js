﻿#pragma strict

var speed = 5;

function Update ()
{
	var amtToMove = speed * Time.deltaTime;
	
	transform.Translate(Vector3.left * amtToMove);

	if(transform.position.x < -37) {
		Destroy(gameObject);
	}
}