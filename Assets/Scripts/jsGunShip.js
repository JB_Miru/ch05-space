﻿#pragma strict

var speed = 20;
var Laser : Transform;
var explosion : Transform;

var sound : AudioClip;
var sound1 : AudioClip;

private var spPoint : GameObject;

function Start () 
{
	spPoint = GameObject.Find("SpawnPoint");
}

function Update () 
{
	var amtToMove = speed * Time.deltaTime;
	var front = Input.GetAxis("Horizontal") * amtToMove;
	var up = Input.GetAxis("Vertical") * amtToMove;

	transform.Translate(Vector3(front, up, 0));
	
	transform.position.x = Mathf.Clamp(transform.position.x, -29, 29);
	transform.position.y = Mathf.Clamp(transform.position.y, -16, 17);

	if(Input.GetButtonDown("Fire1"))
	{
		Instantiate(Laser,spPoint.transform.position, Quaternion.identity);
		//AudioSource.PlayClipAtPoint(sound, transform.position);
	}
}

function OnTriggerEnter(coll : Collider)
{
	if( (jsGameManager.state == STATE.ENTER) ||
		(jsGameManager.state == STATE.WAIT) ||
		(coll.gameObject.tag != "ASTEROID") )
	{
		return;
	}
	
	Instantiate(explosion, transform.position, Quaternion.identity);
	coll.SendMessage("SetAsteroid", SendMessageOptions.DontRequireReceiver);
	//AudioSource.PlayClipAtPoint(sound1, transform.position);
}

function EnterGunShip()
{

}

function Undead()
{
	var child = transform.Find("Space_Shooter");
	
	for(var i=0; i<jsGameManager.undeadCnt; ++i) {
		child.renderer.material.color = Color.red;
		yield WaitForSeconds(0.5);
		
		child.renderer.material.color = Color.white;
		yield WaitForSeconds(0.5);
	}
	
	jsGameManager.undeadCnt = 0;
}